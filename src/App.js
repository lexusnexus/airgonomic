import React, { useState, useEffect } from "react";
import "bootstrap/dist/css/bootstrap.min.css";
import AppNavbar from "./components/AppNavbar";
import Home from "./pages/Home";
import AboutUs from "./pages/AboutUs";
import OurServices from "./pages/OurServices";
import CovidSafety from "./pages/CovidSafety";
import ErrorPage from "./pages/ErrorPage";
import AOS from 'aos';
import 'aos/dist/aos.css';
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";

export default function App() {
    useEffect(() => {
      AOS.init();
      });
   return (
      <Router>
         <AppNavbar />
         <Routes>
            <Route path="/" element={<Home />} />
            <Route path="/about-us" element={<AboutUs />} />
            <Route path="/our-services" element={<OurServices />} />
            <Route path="/covid-19-safety" element={<CovidSafety />} />
            <Route path="*" element={<ErrorPage />} />
         </Routes>
      </Router>
   );
}
