import React, { useState, useEffect } from "react";
import { Container, Row, Col } from "react-bootstrap";
import mission1 from "./../images/mission/mission1.jpg";
import mission2 from "./../images/mission/mission2.png";
export default function Mission() {
   const [windowDimension, setWindowDimension] = useState(null);

   useEffect(() => {
      setWindowDimension(window.innerWidth);
   }, []);

   useEffect(() => {
      function handleResize() {
         setWindowDimension(window.innerWidth);
      }

      window.addEventListener("resize", handleResize);
      return () => window.removeEventListener("resize", handleResize);
   }, []);

   const isMobile = windowDimension <= 768;

   return (
      <Container className=" mb-5 missionContainer">
         <Row className="">
            <Col sm={12} md={6} xs={{ order: 1 }} lg={{ order: 1 }}>
               <Row className="missionRow">
                  <Col xs={2}>
                     <div className="outer4"></div>
                  </Col>
                  <Col xs={10}>
                  {/* data-aos="fade-down-right" data-aos-offset="200" data-aos-easing="ease-in-sine" data-aos-duration="600"*/}
                     <div className="outer">
                        <h3 className="" style={{ color: "white" }}>
                           Our Mission
                        </h3>
                        <img src={mission1} alt="brand1" />
                     </div>
                  </Col>
               </Row>
            </Col>
            <Col className="p-3 p-lg-5" sm={12} md={6} xs={{ order: 2 }} lg={{ order: 2 }}>
               <div className="missionText" data-aos="fade-left" data-aos-offset="200" data-aos-easing="ease-in-sine" data-aos-duration="900">
                  <hr />
                  <p>To provide expert and quality HVAC services such as installation, maintenance and repair and to ensure the comfort of the clients by delivering reliability and availability of its services that they deserve.</p>
               </div>
            </Col>
         </Row>
         {isMobile === false ? (
            <Row className="">
               <Col className="p-3 p-lg-5" sm={12} md={6} xs={{ order: 2 }} sm={{ order: 2 }} lg={{ order: 1 }}>
                  <div className="missionText" data-aos="fade-right" data-aos-offset="200" data-aos-easing="ease-in-sine" data-aos-duration="900">
                     <hr />
                     <p>Aims to be known as one of the best and trusted HVAC services company and recognized as a professional team for its consistency and reliability across the globe.</p>
                  </div>
               </Col>
               <Col sm={12} md={6} xs={{ order: 1 }} sm={{ order: 2 }} lg={{ order: 2 }}>
                  <Row className="missionRow">
                     <Col xs={10}>
                        <div className="outer2">
                           <img src={mission2} alt="brand2" />
                           <h3 style={{ color: "white" }}>Our Vision</h3>
                        </div>
                     </Col>
                     <Col xs={2}>
                        <div className="outer3"></div>
                     </Col>
                  </Row>
               </Col>
            </Row>
         ) : (
            <Row className="">
               <Col xs={12}>
                  <Row className="missionRow">
                     <Col xs={10}>
                        <div className="outer2">
                           <img src={mission2} alt="brand2" />
                           <h3 style={{ color: "white" }}>Our Vision</h3>
                        </div>
                     </Col>
                     <Col xs={2}>
                        <div className="outer3"></div>
                     </Col>
                  </Row>
               </Col>
               <Col xs={12} className="p-3 p-lg-5">
                  <div className="missionText"  data-aos="fade-right" data-aos-offset="200" data-aos-easing="ease-in-sine" data-aos-duration="600">
                     <hr />
                     <p>Aims to be known as one of the best and trusted HVAC services company and recognized as a professional team for its consistency and reliability across the globe.</p>
                  </div>
               </Col>
            </Row>
         )}
      </Container>
   );
}
