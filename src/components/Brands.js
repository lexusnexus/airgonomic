import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import carrier from "./../images/brands/carrier.png";
import lg from "./../images/brands/lg.png";
import panasonic from "./../images/brands/panasonic.png";
import condura from "./../images/brands/condura.png";
import samsung from "./../images/brands/samsung.png";
import kolin from "./../images/brands/kolin.png";
import koppel from "./../images/brands/koppel.png";
import sharp from "./../images/brands/sharp.png";
import fujidenzo from "./../images/brands/fujidenzo.png";
import daikin from "./../images/brands/daikin.png";
import aux from "./../images/brands/auxlogo.png";

export default function Brands() {
   return (
      <Container className="text-center my-5" id="brandsID">
         <h1  data-aos="fade-up" data-aos-offset="200" data-aos-easing="ease-in-sine" data-aos-duration="900">All Brands Serviced</h1>
         <h5  data-aos="fade-up" data-aos-offset="200" data-aos-easing="ease-in-sine" data-aos-duration="900">We offer all essential services for every brand and unit type.</h5>
         <Row className="py-5 justify-content-md-center">
            <Col md={4} lg={3} className="p-3">
               <img src={carrier} alt="brand1" />
            </Col>
            <Col md={4} lg={3} className="p-3">
               <img src={lg} alt="brand1" />
            </Col>
            <Col md={4} lg={3} className="p-3">
               <img src={panasonic} alt="brand1" />
            </Col>
            <Col md={4} lg={3} className="p-3">
               <img src={condura} alt="brand1" />
            </Col>

            <Col md={4} lg={3} className="p-3">
               <img src={samsung} alt="brand1" />
            </Col>
            <Col md={4} lg={3} className="p-3">
               <img src={kolin} alt="brand1" />
            </Col>
            <Col md={4} lg={3} className="p-3">
               <img src={koppel} alt="brand1" />
            </Col>
            <Col md={4} lg={3} className="p-3">
               <img src={sharp} alt="brand1" />
            </Col>

            <Col md={4} lg={3} className="p-3">
               <img src={fujidenzo} alt="brand1" />
            </Col>
            <Col md={4} lg={3} className="p-3">
               <img src={daikin} alt="brand1" />
            </Col>
            <Col md={4} lg={3} className="p-3">
               <img src={aux} alt="brand1" />
            </Col>
         </Row>
      </Container>
   );
}
