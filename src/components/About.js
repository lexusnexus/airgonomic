import React from 'react';
import {
Carousel
} from 'react-bootstrap';
import about1 from "./../images/about1.png"
export default function About() {
return (
<div className="aboutContainer mb-5">
   <div class="head-hero head-about-us">
      <div class="hero-content container">
         <div class="row justify-content-md-center h-inherit">
            <div class="col-lg-8 col-12 align-center my-auto padtop-about-m" data-aos="zoom-in-up" data-aos-offset="200" data-aos-easing="ease-in-sine" data-aos-duration="900">
               <h1 class="text-white centered">"Our air care is beyond compare."</h1>
            </div>
         </div>
      </div>
   </div>
   <p  data-aos="fade-right"  data-aos-duration="900" class="aboutText mx-3 my-5 my-md-3 mx-md-5 my-lg-5"x>Airgonomic Airconditioning Services is a DTI Registered business based in Biñan City,Laguna
      Philippines, providing quality and safe HVAC services. Airgonomic from the word ergonomic,
      provides comfort and quality that our client deserves.
   </p>
   <hr/>
   <p  data-aos="fade-left" data-aos-offset="100" data-aos-easing="ease-in-sine" data-aos-duration="900" class="aboutText mx-3 my-5 my-md-3 mx-md-5 my-lg-5">
      We offer expert services such as installation, maintenance and repair of air conditioning units
      at your door step at an affordable price. Airgonomic employees undergo avant-garde training
      and has certifications to ensure that the quality of service we offer to our clients is reliable.
   </p>
</div>
)
}