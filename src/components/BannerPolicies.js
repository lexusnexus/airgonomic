import React from "react";
import policies from "./../images/policies/policies.png";

export default function BannerPolicies() {
   return (
      <div>
         <div id="overlay"></div>
         <div className="bannerServiceContainer">
            <div class="head-services-container">
               <div class="head-services-wrap">
                  <div class="head-services-content"  data-aos="fade-right"  data-aos-duration="1500">
                     <h1>
                        Our COVID-19 <br /> Safety Measures
                     </h1>
                     <p>Quality service for your air conditioning needs.</p>
                  </div>
                  <img  data-aos="fade-down-left"  data-aos-duration="1500" src={policies} alt="Banner Policies" class="img-fluid head-services-img lazyloaded imageServ" />
               </div>
            </div>
         </div>
      </div>
   );
}
