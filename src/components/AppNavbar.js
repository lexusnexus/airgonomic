import React, { useContext, useEffect, useState } from "react";
import { Nav, Navbar, Container, Offcanvas } from "react-bootstrap";
import { NavLink } from "react-router-dom";
import airgo from "./../images/airgo.png";
import airgowhite from "./../images/airgowhite.png";
export default function AppNavbar() {
   const [show, setShow] = useState(false);
   const [navbar, setNavbar] = useState(false);
   let logoBlue = airgo;
   let logoWhite = airgowhite;
   const [logo, setLogo] = useState(airgo);

   useEffect(() => {
      window.location.pathname === "/our-services" || window.location.pathname === "/covid-19-safety" || window.location.pathname === "/about-us" ? setLogo(logoWhite) : setLogo(logoBlue);
   }, []);

   const changeBackground = () => {
      if (window.scrollY >= 20) {
         setNavbar(true);
         setLogo(airgo);
      } else {
         setNavbar(false);
         if (window.location.pathname === "/our-services" || window.location.pathname === "/covid-19-safety" || window.location.pathname === "/about-us") {
            setLogo(airgowhite);
         } else {
            setLogo(airgo);
         }
      }
   };

   const [windowDimension, setWindowDimension] = useState(null);

   useEffect(() => {
      setWindowDimension(window.innerWidth);
   }, []);

   useEffect(() => {
      function handleResize() {
         setWindowDimension(window.innerWidth);
      }

      window.addEventListener("resize", handleResize);
      return () => window.removeEventListener("resize", handleResize);
   }, []);

   const isMobile = windowDimension <= 991;
   window.addEventListener("scroll", changeBackground);
   return isMobile === false ? (
      <Navbar fixed="top" variant={logo === airgowhite ? "dark" : "light"} expand="lg" className={navbar ? "navtext active" : "navtext"}>
         <Container>
            <Navbar.Brand href="/">
               <img class="mx-1 navlogo" src={logo} alt="logo" height="50px" />
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="basic-navbar-nav" />
            <Navbar.Collapse id="basic-navbar-nav">
               <Nav className="me-auto">
                  <Nav.Link href="/our-services" className={window.location.pathname === "/our-services" ? "navlink active" : "navlink"}>
                     Our Services
                  </Nav.Link>
                  <Nav.Link href="/about-us" className={window.location.pathname === "/about-us" ? "navlink active" : "navlink"}>
                     About Us
                  </Nav.Link>
                  <Nav.Link href="/covid-19-safety" className={window.location.pathname === "/covid-19-safety" ? "navlink active" : "navlink"}>
                     COVID-19 Safety
                  </Nav.Link>
               </Nav>
            </Navbar.Collapse>
         </Container>
      </Navbar>
   ) : (
      <Navbar fixed="top" variant={logo === airgowhite ? "dark" : "light"} expand={false} className={navbar ? "navtext active" : "navtext"}>
         <Container>
            <Navbar.Brand href="/">
               <img class="mx-1 navlogo" src={logo} alt="logo" height="50px" />
            </Navbar.Brand>
            <Navbar.Toggle aria-controls="offcanvasNavbar" />
            <Navbar.Offcanvas id="offcanvasNavbar" aria-labelledby="offcanvasNavbarLabel" placement="end">
               <Offcanvas.Header closeButton>
                  <Offcanvas.Title id="offcanvasNavbarLabel">
                     <img class="mx-1 navlogo" src={logoWhite} alt="logo" height="40px" />
                  </Offcanvas.Title>
               </Offcanvas.Header>
               {/*<hr/>*/}
               <Offcanvas.Body>
                  <Nav className="me-auto offNav">
                     <hr />
                     <Nav.Link href="/" className={window.location.pathname === "/" ? "navlink active" : "navlink"}>
                        Home
                     </Nav.Link>
                     <hr />
                     <Nav.Link href="/our-services" className={window.location.pathname === "/our-services" ? "navlink active" : "navlink"}>
                        Our Services
                     </Nav.Link>
                     <hr />
                     <Nav.Link href="/about-us" className={window.location.pathname === "/about-us" ? "navlink active" : "navlink"}>
                        About Us
                     </Nav.Link>
                     <hr />
                     <Nav.Link href="/covid-19-safety" className={window.location.pathname === "/covid-19-safety" ? "navlink active" : "navlink"}>
                        COVID-19 Safety
                     </Nav.Link>
                     <hr />
                  </Nav>
               </Offcanvas.Body>
            </Navbar.Offcanvas>
         </Container>
      </Navbar>
   );
}
