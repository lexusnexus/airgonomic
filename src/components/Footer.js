import React from "react";

export default function Footer() {
   return (
      <div class="" id="footer">
         <div class="row">
            <div class="col-md-6 col-sm-12 col-12">
               <p>
                  <strong>Airgonomic Airconditioning Services</strong> is a top-quality registered HVAC cleaning company. We offer all manner of services to our clients. Our services range from maintenance to the retailing of
                  air-conditioning and ventilation systems.
               </p>
            </div>

            <div class="col-md-2 col-sm-6 col-6">
               <p class="footer-title">Company</p>
               <ul class="footer-ul">
                  <li class="footer-list">
                     <a href="/about-us" title="About Us" class="footer-link">
                        About Us
                     </a>{" "}
                  </li>
                  <li class="footer-list">
                     <a href="/our-services" title="Services" class="footer-link">
                        Services
                     </a>{" "}
                  </li>
               </ul>
            </div>

            <div class="col-md-2 col-sm-6 col-6 ">
               <p class="footer-title">Customers</p>
               <ul class="footer-ul">
                  <li class="footer-list">
                     <a href="/about-us#clientID" title="Dealer" class="footer-link">
                        Major Clients
                     </a>{" "}
                  </li>
               </ul>
            </div>

            <div class="col-md-2 col-sm-6 col-6 pt-3 pt-md-0">
               <p class="footer-title">Get Help</p>
               <ul class="footer-ul">
                  <li class="footer-list">
                     <a href="/about-us#contactID" title="Contact Us" class="footer-link">
                        Contact Us
                     </a>{" "}
                  </li>
                  <li class="footer-list">
                     <a href="/our-services#brandsID" title="Frequently Ask Questions" class="footer-link">
                        Brands
                     </a>{" "}
                  </li>
                  <li class="footer-list">
                     <a href="/covid-19-safety" title="Covid-19 Safety Measures" class="footer-link">
                        COVID-19 Safety
                     </a>{" "}
                  </li>
               </ul>
            </div>
         </div>

         <div class="row justify-content-md-center mobile-hidden tablet-hidden pt-3">
            <div class="col align-self-end">
            <div class="row">
            <div class="social f-left pb-2">
                  <span>© 2022 Airgonomic Airconditioning Services - All Rights Reserved</span>
               </div>
               </div>
                <div class="row">
            <div class="social f-left pb-2">
                  <span>Coded by <a href="https://www.linkedin.com/in/melaithecoder/" target="_blank" class="social-icons" rel="noopener noreferrer">
                         melaniedotollo@gmail.com
                     </a></span>
               </div>
               </div>
            </div>
            <div class="col align-self-end">
               <div class="social f-right">
                  <a href="https://www.facebook.com/airconcleaningbinan/" target="_blank" class="social-fb px-3" rel="noopener noreferrer">
                     <i class="fa-brands fa-facebook fa-lg footerIcon"></i>
                  </a>

                  <a href="https://www.instagram.com/airgonomic.services/" target="_blank" class="social-twitter" rel="noopener noreferrer">
                     <i class="fa-brands fa-instagram fa-lg footerIcon"></i>
                  </a>
               </div>
            </div>
         </div>
      </div>
   );
}
