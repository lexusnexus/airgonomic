import React from "react";

import { Carousel } from "react-bootstrap";
import air1 from "./../images/airgo5.png";
import air2 from "./../images/airgo2.png";
import air3 from "./../images/airgo6.png";
export default function Banner() {
   return (
      <div>
         <Carousel fade indicators={false} controls={false}>
            <Carousel.Item interval={4000}>
               <div className="bannerImg"></div>
               <Carousel.Caption className="text-left">
                  <div class=" p-0 p-md-5 rounded-lg m-3" data-aos="fade-up" data-aos-offset="200" data-aos-easing="ease-in-sine" data-aos-duration="900">
                     <h1 class="display-4">
                        Professional Aircon <br />
                        Technicians
                     </h1>
                     <p class="lead">Quality service for your air conditioning needs.</p>
                     <a class="btn btn-airgo btn-lg px-3" href="#highlightsID" role="button">
                        Learn more
                     </a>
                  </div>
               </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item interval={4000}>
               <div className="bannerImg2"></div>
               <Carousel.Caption className="text-left">
                  <div class=" p-0 p-md-5 rounded-lg m-3">
                     <h1 class="display-4 d-none d-md-block">
                        Expert Aircon <br />
                        Technicians
                     </h1>
                     <h1 class="display-4 d-sm-block d-md-none">
                        Expert <br />
                        Aircon <br />
                        Technicians
                     </h1>
                     <p class="lead">Quality service for your air conditioning needs.</p>
                     <a class="btn btn-airgo btn-lg px-3" href="#highlightsID" role="button">
                        Learn more
                     </a>
                  </div>
               </Carousel.Caption>
            </Carousel.Item>
            <Carousel.Item interval={4000}>
               <div className="bannerImg3"></div>
               <Carousel.Caption className="text-left">
                  <div class=" p-0 p-md-5 rounded-lg m-3">
                     <h1 class="display-4 d-none d-md-block">
                        Reliable Aircon <br />
                        Technicians
                     </h1>
                     <h1 class="display-4 d-sm-block d-md-none">
                        Reliable <br />
                        Aircon <br />
                        Technicians
                     </h1>
                     <p class="lead">Quality service for your air conditioning needs.</p>
                     <a class="btn btn-airgo btn-lg px-3" href="#highlightsID" role="button">
                        Learn more
                     </a>
                  </div>
               </Carousel.Caption>
            </Carousel.Item>
         </Carousel>
      </div>
   );
}
