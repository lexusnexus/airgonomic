import React from "react";
import service from "./../images/service.png";

export default function BannerService() {
   return (
      <div className="bannerServiceContainer">
         <div class="head-services-container">
            <div class="head-services-wrap">
               <div class="head-services-content"  data-aos="fade-right"  data-aos-duration="1500">
                  <h1>
                     Professional Aircon <br /> Technicians
                  </h1>
                  <p>Quality service for your air conditioning needs.</p>
                  <a class="btn js-link btn-shadow btn-lg mt-2 btn-head-white here" href="#typesID">
                     Learn more
                  </a>
               </div>
               <img  data-aos="fade-down-left"  data-aos-duration="1500" src={service} class="img-fluid head-services-img lazyloaded imageServ" alt="aircon service" />
            </div>
         </div>
      </div>
   );
}
