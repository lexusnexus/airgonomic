import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import our from "./../images/our.jpg";

export default function Services() {
   return (
      <Container className=" my-5">
         <Row className="servRow  blue-bg">
            <Col xs={12} md={7} className="servColImg"></Col>
            <Col xs={12} md={5} className="servColList"  data-aos="fade-left"  data-aos-duration="1500">
               <h3>Our Aircon Services</h3>
               <ul id="check">
                  <li>Air conditioning and refrigeration cleaning maintenance</li>
                  <li>Air conditioning and refrigeration repair</li>
                  <li>Air conditioning dismantling and relocation</li>
                  <li>Air conditioning system installation</li>
                  <li>HVAC maintenance and repairs</li>
                  <li>Duct work (e.g. cooling, exhaust, heating and ventilation) installation</li>
                  <li>Heating and ventilation system component installation</li>
               </ul>
            </Col>
         </Row>
      </Container>
   );
}
