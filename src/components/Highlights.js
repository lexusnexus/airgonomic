import React from "react";
import { Container, Card, Button, Row, Col } from "react-bootstrap";
import aircon from "./../images/aircon.jpg";
import work1 from "./../images/work1.jpg";
import work2 from "./../images/work2.jpg";
import work3 from "./../images/work3.jpg";

export default function Highlights() {
   return (
      <Container fluid id="high">
         <h2 class="text-center py-3">On-site Services</h2>
         <Row className="justify-content-md-center">
            <Col className="col-2">
               <Card style={{ width: "11rem" }}>
                  <Card.Img variant="top" src={aircon} />
                  <Card.Body>
                     <Card.Title className="text-center">Aircon</Card.Title>
                  </Card.Body>
               </Card>
            </Col>
            <Col className="col-2">
               <Card style={{ width: "11rem" }}>
                  <Card.Img variant="top" src={aircon} />
                  <Card.Body>
                     <Card.Title className="text-center">Aircon</Card.Title>
                  </Card.Body>
               </Card>
            </Col>
            <Col className="col-2">
               <Card style={{ width: "11rem" }}>
                  <Card.Img variant="top" src={aircon} />
                  <Card.Body>
                     <Card.Title className="text-center">Aircon</Card.Title>
                  </Card.Body>
               </Card>
            </Col>
            <Col className="col-2">
               <Card style={{ width: "11rem" }}>
                  <Card.Img variant="top" src={aircon} />
                  <Card.Body>
                     <Card.Title className="text-center">Aircon</Card.Title>
                  </Card.Body>
               </Card>
            </Col>
         </Row>
         <h3 class="text-center pt-5 pb-3">We Stand Behind Our Work</h3>
         <Row className="justify-content-md-center">
            <Col className="col-2">
               <Card style={{ height: "30rem" }}>
                  <Card.Img variant="top" class="img-fluid lazyloaded" src={work1} style={{ height: "15rem" }} />
                  <Card.Body>
                     <Card.Title className="text-center">Professional</Card.Title>
                     <Card.Text>Developing detailed knowledge into action that helps our clients with their needs regarding HVAC Services.</Card.Text>
                  </Card.Body>
               </Card>
            </Col>
            <Col className="col-2">
               <Card style={{ height: "30rem" }}>
                  <Card.Img variant="top" class="img-fluid lazyloaded" src={work2} style={{ height: "15rem" }} />
                  <Card.Body>
                     <Card.Title className="text-center">Expert</Card.Title>
                     <Card.Text>Proficient in the field of HVAC Services with extensive ability based on research and experience.</Card.Text>
                  </Card.Body>
               </Card>
            </Col>
            <Col className="col-2">
               <Card style={{ height: "30rem" }}>
                  <Card.Img variant="top" class="img-fluid lazyloaded" src={work3} style={{ height: "15rem" }} />
                  <Card.Body>
                     <Card.Title className="text-center">Reliable</Card.Title>
                     <Card.Text>Trusted air conditioning contractor that is consistent in quality of HVAC Services. </Card.Text>
                  </Card.Body>
               </Card>
            </Col>
         </Row>
      </Container>
   );
}
