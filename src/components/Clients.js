import React from "react";
import ostrea from "./../images/client/ostrea.png";
import agx from "./../images/client/agx.png";
import showa from "./../images/client/showa.png";
import metal from "./../images/client/metal.png";
import kabisig from "./../images/client/kabisig.png";
import mgg from "./../images/client/mgg.png";
import deped from "./../images/client/deped.png";
import autobody from "./../images/client/autobody.png";
import sm from "./../images/client/sm.png";
import smart from "./../images/client/smart.png";

export default function Clients() {
   return (
      <div class="container clientContainer" id="clientID">
         <div class="row justify-content-md-center">
            <div class="col-lg-12 col-md-12 col-sm-12 col-12 align-center mx-0">
               <h2 class=" text-blue" data-aos="fade-up"  data-aos-duration="1500">Take a look at our clients</h2>
            </div>
         </div>

         <div class="row justify-content-md-center types">
            <div class="col-lg-3 col-md-6 col-12 align-center clientDiv">
               <img src={ostrea} class="img-fluid lazyloaded clientImg" alt="window type aircon" />
               <div className="middle">
                  <div className="clientText">Ostrea Mineral Laboratories Inc.</div>
               </div>
            </div>

            <div class="col-lg-3 col-md-6 col-12 align-center clientDiv">
               <img src={agx} class="img-fluid lazyloaded clientImg" alt="split type aircon" />
               <div className="middle">
                  <div className="clientText">AGX Express Phils Inc.</div>
               </div>
            </div>

            <div class="col-lg-3 col-md-6 col-12 align-center clientDiv">
               <img src={showa} class="img-fluid lazyloaded clientImg" alt="tower type aircon" />
               <div className="middle">
                  <div className="clientText">Showa Polymer Process Corporation – SPPC</div>
               </div>
            </div>

            <div class="col-lg-3 col-md-6 col-12 align-center clientDiv">
               <img src={metal} class="img-fluid lazyloaded clientImg" alt="cassette type aircon" />
               <div className="middle">
                  <div className="clientText">Metalcrest Technologies Inc.</div>
               </div>
            </div>

            <div class="col-lg-3 col-md-6 col-12 align-center clientDiv">
               <img src={kabisig} class="img-fluid lazyloaded clientImg" alt="suspended type aircon" />
               <div className="middle">
                  <div className="clientText">Kabisig Workers Cooperative</div>
               </div>
            </div>

            <div class="col-lg-3 col-md-6 col-12 align-center clientDiv">
               <img src={mgg} class="img-fluid lazyloaded clientImg" alt="concealed type aircon" />
               <div className="middle">
                  <div className="clientText">MGG Oil Apothecary, OPC</div>
               </div>
            </div>
            <div class="col-lg-3 col-md-6 col-12 align-center clientDiv">
               <img src={deped} class="img-fluid lazyloaded clientImg" alt="chiller type aircon" />
               <div className="middle">
                  <div className="clientText">DepEd (Department of Education)</div>
               </div>
            </div>

            <div class="col-lg-3 col-md-6 col-12 align-center clientDiv">
               <img src={autobody} class="img-fluid lazyloaded  clientImg" alt="suspended type aircon" />
               <div className="middle">
                  <div className="clientText">Autobody Technologies Corp.</div>
               </div>
            </div>

            <div class="col-lg-3 col-md-6 col-12 align-center clientDiv">
               <img src={sm} class="img-fluid lazyloaded clientImg" alt="concealed type aircon" />
               <div className="middle">
                  <div className="clientText">SM City Sucat</div>
               </div>
            </div>
            <div class="col-lg-3 col-md-6 col-12 align-center clientDiv">
               <img src={smart} class="img-fluid lazyloaded clientImg" alt="chiller type aircon" />
               <div className="middle">
                  <div className="clientText">Smart Communication Inc. Cabuyao</div>
               </div>
            </div>
         </div>
      </div>
   );
}
