import React from "react";
import { Container, Row, Col } from "react-bootstrap";

import distancing from "./../images/policies/distancing.png";
import clean from "./../images/policies/clean.png";
import mask from "./../images/policies/mask.png";
import oneperson from "./../images/policies/oneperson.png";
export default function Policies() {
   return (
      <Container className=" policiesContainer text-center pb-5">
         <h3 className="pb-5 mt-5">Important Safety Measures Against COVID-19 During Our Service</h3>
         <Row className="">
            <Col  data-aos="fade-down-right"  data-aos-duration="1500">
               <img src={distancing} alt="brand1" />
               <p class="p-responsive px-lg-5 mx-5 mb-5 mb-md-0">
                  Limit interaction and maintain at least a <strong>2 meter distance</strong> from our partner technician at all times.
               </p>
            </Col>
            <Col  data-aos="fade-down-left"  data-aos-duration="1500">
               <img src={mask} alt="brand1" />
               <p class="p-responsive px-lg-5 mx-5 mb-5 mb-md-0">
                  At minimum, wear a <strong>mask and gloves</strong> when interacting with or handing any item to our partner technician and <strong>sanitize</strong> immediately.
               </p>
            </Col>
         </Row>
         <Row className="">
            <Col  data-aos="fade-up-right"  data-aos-duration="1500">
               <img src={oneperson} alt="brand1" />
               <p class="p-responsive px-lg-5 mx-5 mb-5 mb-md-0">
                  Technician should only interact with <strong>1 household member</strong> during service.
               </p>
            </Col>
            <Col  data-aos="fade-up-left"  data-aos-duration="1500">
               <img src={clean} alt="brand1" />
               <p class="p-responsive px-lg-5 mx-5 mb-5 mb-md-0">
                  Technician <strong>thoroughly sanitizes</strong> the work area after the service.
               </p>
            </Col>
         </Row>
      </Container>
   );
}
