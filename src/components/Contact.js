import React from "react";
import Iframe from "react-iframe";

import { Row, Col, Container } from "react-bootstrap";

export default function Map() {
   return (
      <Container className="contactContainer" id="contactID">
         <h2 className="text-center">Contact Us</h2>
         <Row className=" justify-content-center" id="map">
            <Col xs={12} md={4} lg={4} className=" pt-5 pt-xs-1 info">
               <h5>Our location:</h5>
               <Row>
                  <Col xs={2} className="text-center">
                     <i class="fa-solid fa-location-dot fa-lg"></i>
                  </Col>
                  <Col xs={10}>
                     <p>CBM Bldg. Mondo Bambini Jubilation Biñan, Laguna</p>
                  </Col>
               </Row>

               <h5>Contact us here:</h5>
               <Row>
                  <Col xs={2} className="text-center">
                     <i class="fas fa-mobile-alt fa-lg" aria-hidden="true"></i>
                  </Col>
                  <Col xs={10}>
                     <p>0906-239-7512</p>
                  </Col>
                  <Col xs={2} className="text-center">
                     <i class="fas fa-mobile-alt fa-lg" aria-hidden="true"></i>
                  </Col>
                  <Col xs={10}>
                     <p>0956-845-4089</p>
                  </Col>
                  <Col xs={2} className="text-center">
                     <i class="fas fa-phone-alt fa-lg" aria-hidden="true"></i>{" "}
                  </Col>
                  <Col xs={10}>
                     <p>(049) 539-2573</p>
                  </Col>
               </Row>

               <h5>Email Address:</h5>
               <Row>
                  <Col xs={2} className="text-center ">
                     <i class="fa-solid fa-envelope fa-lg"></i>
                  </Col>
                  <Col xs={10}>
                     <p className="emailText d-none d-md-none d-lg-none d-xl-block">airgonomic.hvacservices@gmail.com</p>
                     <p className="emailText d-sm-block d-md-none  d-lg-block d-xl-none">
                        airgonomic.hvacservices
                        <br />
                        @gmail.com
                     </p>
                     <p className="emailText d-none d-sm-none d-md-block  d-lg-none d-xl-none">
                        airgonomic
                        <br />
                        .hvacservices
                        <br />
                        @gmail.com
                     </p>
                  </Col>
               </Row>

               <h5>Social Media Accounts:</h5>
               <Row>
                  <Col xs={2} className="text-center">
                     <i class="fa-brands fa-facebook fa-lg"></i>
                  </Col>
                  <Col xs={10}>
                     <p>
                        <a href="https://www.facebook.com/airconcleaningbinan/" target="_blank">
                           Airgonomic Airconditioning Services
                        </a>
                     </p>
                  </Col>
                  <Col xs={2} className="text-center">
                     <i class="fa-brands fa-instagram fa-lg"></i>
                  </Col>
                  <Col xs={10}>
                     <p>
                        <a href="https://www.instagram.com/airgonomic.services/" target="_blank">
                           airgonomic.services
                        </a>
                     </p>
                  </Col>
               </Row>
            </Col>
            <Col xs={12} md={8} lg={7} className=" pb-5 mt-5 mt-xs-1 mapplace">
               <Iframe
                  src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3865.955940156558!2d121.08549251481521!3d14.313992089985522!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3397d9c8da09d609%3A0xb09ce1d78c260ef6!2sAirgonomic%20Airconditioning%20Services!5e0!3m2!1sen!2sph!4v1649964737764!5m2!1sen!2sph"
                  width="100%"
                  height="100%"
                  style="border:0;"
                  allowfullscreen=""
                  loading="lazy"
                  referrerpolicy="no-referrer-when-downgrade"
               />
            </Col>
         </Row>
      </Container>
   );
}
