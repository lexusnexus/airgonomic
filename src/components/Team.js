import React from "react";
import { Carousel } from "react-bootstrap";
import team from "./../images/meet.png";

export default function Team() {
   return (
      <div id="team" class="pt-5"  >
         <h2 class="text-center " data-aos="fade-up"  data-aos-duration="1500">Meet Our Team</h2>
         <p className="teamParagraph mx-0 mx-md-5 text-center" data-aos="fade-up"  data-aos-duration="1500">
            Airgonomic Team consists of professionals, experts and reliable members that focuses on the quality of air conditioning services and supporting the growth of each member of the team.
         </p>

         <img class="center img-fluid " src={team} alt="Team" />
      </div>
   );
}
