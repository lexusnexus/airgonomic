import React from "react";
import windowType from "./../images/aircon/window-type-aircon.png";
import split from "./../images/aircon/split-type-aircon.png";
import tower from "./../images/aircon/tower-type-aircon.png";
import cassette from "./../images/aircon/cassette-type-aircon.png";
import suspended from "./../images/aircon/suspended-type-aircon.png";
import concealed from "./../images/aircon/concealed-type-aircon.png";
import chiller from "./../images/aircon/chiller-type-aircon.png";

export default function Types() {
   return (
      <div class="container pt-5" id="typesID"  >
         <div class="row justify-content-md-center" >
            <div class="col-lg-12 col-md-12 col-sm-12 col-12 align-center mx-0">
               <h1 class="h1-headline text-blue" data-aos="fade-up"  data-aos-duration="1500">Servicing All Aircon Brands and Types</h1>
            </div>
            <div class="col-lg-12 col-md-12 col-sm-6 col-12 align-center mb-5">
               <p class="no-my" data-aos="fade-up"  data-aos-duration="1500">
                  Our aircon technicians are expert in handling residential and light commercial types of all major brands <br /> including Carrier, Panasonic, Condura, Kolin, Samsung, and LG.
               </p>
            </div>
         </div>

         <div class="row justify-content-md-center types">
            <div class="col-lg-3 col-md-6 col-12 align-center mt-2 mb-2">
               <img src={windowType} class="img-fluid lazyloaded" alt="window type aircon" />
               <span class="title-blue-bg">Window Type</span>
            </div>

            <div class="col-lg-3 col-md-6 col-12 align-center mt-2 mb-2">
               <img src={split} class="img-fluid lazyloaded" alt="split type aircon" />
               <span class="title-blue-bg">Split Type</span>
            </div>

            <div class="col-lg-3 col-md-6 col-12 align-center mt-2 mb-2">
               <img src={tower} class="img-fluid lazyloaded" alt="tower type aircon" />
               <span class="title-blue-bg">Tower Type</span>
            </div>

            <div class="col-lg-3 col-md-6 col-12 align-center mt-2 mb-2">
               <img src={cassette} class="img-fluid lazyloaded" alt="cassette type aircon" />
               <span class="title-blue-bg">Cassette Type</span>
            </div>

            <div class="col-lg-4 col-md-6 col-12 align-center mt-2 mb-2">
               <img src={suspended} class="img-fluid lazyloaded" alt="suspended type aircon" />
               <span class="title-blue-bg">Suspended Type</span>
            </div>

            <div class="col-lg-4 col-md-6 col-12 align-center mt-2 mb-2">
               <img src={concealed} class="img-fluid lazyloaded" alt="concealed type aircon" />
               <span class="title-blue-bg">Concealed Type</span>
            </div>
            <div class="col-lg-4 col-md-6 col-12 align-center mt-2 mb-2">
               <img src={chiller} class="img-fluid lazyloaded" alt="chiller type aircon" />
               <span class="title-blue-bg">Chiller Type</span>
            </div>
         </div>
      </div>
   );
}
