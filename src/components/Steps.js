import React from "react";
import { Container, Row, Col, Card, Button } from "react-bootstrap";

import distancing from "./../images/policies/distancing.png";
import clean from "./../images/policies/clean.png";
import mask from "./../images/policies/mask.png";
import oneperson from "./../images/policies/oneperson.png";
export default function Steps() {
   return (
      <Container className=" my-3 stepsContainer text-center">
         <div class="row justify-content-md-center mb-5">
            <div class="col-md-auto align-center">
               <h4 class="h4-headline font-600 text-dark-blue">We highly encourage all our partner technicians to</h4>
            </div>
            <div class="col-md-9 d-flex align-items-center">
               <h1 class="my-0 px-4 text-dark-blue h1-headline font-900">1</h1>
               <h4 class="h4-sub">Follow their health conditions daily with temperature checks.</h4>
            </div>
            <div class="col-md-9 d-flex align-items-center">
               <h1 class="my-0 px-4 text-dark-blue h1-headline font-900">2</h1>
               <h4 class="h4-sub">Respect the guidelines at each location and follow their additional measures.</h4>
            </div>
            <div class="col-md-9 d-flex align-items-center">
               <h1 class="my-0 px-4 text-dark-blue h1-headline font-900">3</h1>
               <h4 class="h4-sub">Protect themselves at all times and sanitize at the start and end of each service.</h4>
            </div>
         </div>
      </Container>
   );
}
