import React from "react";
import { Container, Row, Col, Card, Carousel, Button } from "react-bootstrap";
import feedback1 from "./../images/feedbacks/feedback1.png";
import feedback2 from "./../images/feedbacks/feedback2.png";
import feedback3 from "./../images/feedbacks/feedback3.png";
import feedback4 from "./../images/feedbacks/feedback4.png";
import feedback5 from "./../images/feedbacks/feedback5.png";
import feedback6 from "./../images/feedbacks/feedback6.png";
import feedback7 from "./../images/feedbacks/feedback7.png";
import feedback8 from "./../images/feedbacks/feedback8.png";
import feedback9 from "./../images/feedbacks/feedback9.png";
import feedback10 from "./../images/feedbacks/feedback10.png";
import feedback11 from "./../images/feedbacks/feedback11.png";
import feedback12 from "./../images/feedbacks/feedback12.png";
import feedback13 from "./../images/feedbacks/feedback13.png";
export default function Testimonials() {
   return (
      <Container className="testimonialContainer bgtesti" id="highlightsID">
         <h1 className="align-center mx-5 mt-5" data-aos="fade-down" data-aos-offset="200" data-aos-easing="ease-in-sine" data-aos-duration="800">What Our Clients Say</h1>
         <Carousel /*fade */ /*indicators={false} */ controls={true}>
            <Carousel.Item interval={4000}>
               <Row className="testimonialRow">
                  <Col md={6}>
                     <Card border="info" className="my-3 shadow-sm">
                        <Card.Body className="align-center">
                           <img src={feedback1} alt="brand1" class="img-fluid" />
                        </Card.Body>
                     </Card>
                  </Col>
                  <Col md={6}>
                     <Card border="info" className="my-3 shadow-sm">
                        <Card.Body className="align-center">
                           <img src={feedback2} alt="brand1" class="img-fluid" />
                        </Card.Body>
                     </Card>
                  </Col>
               </Row>
            </Carousel.Item>

            <Carousel.Item interval={4000}>
               <Row className="testimonialRow">
                  <Col md={6}>
                     <Card border="info" className="my-3 shadow-sm">
                        <Card.Body className="align-center">
                           <img src={feedback3} alt="brand1" class="img-fluid" />
                        </Card.Body>
                     </Card>
                  </Col>
                  <Col md={6}>
                     <Card border="info" className="my-3 shadow-sm">
                        <Card.Body className="align-center">
                           <img src={feedback4} alt="brand1" class="img-fluid" />
                        </Card.Body>
                     </Card>
                  </Col>
               </Row>
            </Carousel.Item>
            <Carousel.Item interval={4000}>
               <Row className="testimonialRow">
                  <Col md={6}>
                     <Card border="info" className="my-3 shadow-sm">
                        <Card.Body className="align-center">
                           <img src={feedback5} alt="brand1" class="img-fluid" />
                        </Card.Body>
                     </Card>
                  </Col>
                  <Col md={6}>
                     <Card border="info" className="my-3 shadow-sm">
                        <Card.Body className="align-center">
                           <img src={feedback6} alt="brand1" class="img-fluid" />
                        </Card.Body>
                     </Card>
                  </Col>
               </Row>
            </Carousel.Item>
            <Carousel.Item interval={4000}>
               <Row className="testimonialRow">
                  <Col md={6}>
                     <Card border="info" className="my-3 shadow-sm">
                        <Card.Body className="align-center">
                           <img src={feedback7} alt="brand1" class="img-fluid" />
                        </Card.Body>
                     </Card>
                  </Col>
                  <Col md={6}>
                     <Card border="info" className="my-3 shadow-sm">
                        <Card.Body className="align-center">
                           <img src={feedback8} alt="brand1" class="img-fluid" />
                        </Card.Body>
                     </Card>
                  </Col>
               </Row>
            </Carousel.Item>
            <Carousel.Item interval={4000}>
               <Row className="testimonialRow">
                  <Col md={6}>
                     <Card border="info" className="my-3 shadow-sm">
                        <Card.Body className="align-center">
                           <img src={feedback11} alt="brand1" class="img-fluid" />
                        </Card.Body>
                     </Card>
                  </Col>
                  <Col md={6}>
                     <Card border="info" className="my-3 shadow-sm">
                        <Card.Body className="align-center">
                           <img src={feedback10} alt="brand1" class="img-fluid" />
                        </Card.Body>
                     </Card>
                  </Col>
               </Row>
            </Carousel.Item>
            <Carousel.Item interval={4000}>
               <Row className="testimonialRow">
                  <Col md={6}>
                     <Card border="info" className="my-3 shadow-sm">
                        <Card.Body className="align-center">
                           <img src={feedback9} alt="brand1" class="img-fluid" />
                        </Card.Body>
                     </Card>
                  </Col>
                  <Col md={6}>
                     <Card border="info" className="my-3 shadow-sm">
                        <Card.Body className="align-center">
                           <img src={feedback13} alt="brand1" class="img-fluid" />
                        </Card.Body>
                     </Card>
                  </Col>
               </Row>
            </Carousel.Item>
         </Carousel>
      </Container>
   );
}
