import React from 'react';
import BannerPolicies from './../components/BannerPolicies';
import Footer from './../components/Footer';
import Policies from './../components/Policies';
import {Container} from 'react-bootstrap';

export default function CovidSafety() {
	return (
		<Container fluid className="p-0 covidPage">
			<BannerPolicies/>
			<Policies/>
			<Footer/>
		</Container>
		)
}