import React from 'react';
import Banner from './../components/Banner';
import Testimonials from './../components/Testimonials';
import Brands from './../components/Brands';
import Footer from './../components/Footer';
import {Container} from 'react-bootstrap';

export default function Home() {
	return (
		<Container fluid className="p-0 homePage">
			<Banner/>
			<Testimonials/>
			<Brands/>
			<Footer/>
		</Container>
		)
}