import React from 'react';
import About from './../components/About';
import Team from './../components/Team';
import Mission from './../components/Mission';
import Clients from './../components/Clients';
import Footer from './../components/Footer';
import Contact from './../components/Contact';
import {Container} from 'react-bootstrap';

export default function Home() {
	return (
		<Container fluid className="p-0 aboutPage">
			<About/>
			<Team/>
			<Mission/>
			<Clients/>
			<Contact/>
			<Footer/>
		</Container>
		)
}