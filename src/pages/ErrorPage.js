import React, { useState, useEffect } from "react";
import { Container, Row, Col } from "react-bootstrap";

export default function ErrorPage() {
   const [windowDimension, setWindowDimension] = useState(null);

   useEffect(() => {
      setWindowDimension(window.innerWidth);
   }, []);

   useEffect(() => {
      function handleResize() {
         setWindowDimension(window.innerWidth);
      }

      window.addEventListener("resize", handleResize);
      return () => window.removeEventListener("resize", handleResize);
   }, []);

   const isMobile = windowDimension <= 767;

   return isMobile === false ? (
      <Container className="my-5 errorPage">
         <Row className="">
            <Col xs={12} md={5} xs={{ order: 2 }} md={{ order: 1 }} className="text-center d-flexError">
               <h1>OOPS!</h1>
               <h3>We can't seem to find the page you're looking for.</h3>
               <h6>Error code: 404</h6>
               <a class="btn btn-airgo btn-lg px-3" href="/" role="button">
                  Back to Home Page
               </a>
            </Col>
            <Col xs={12} md={7} xs={{ order: 1 }} md={{ order: 2 }} className="errorImg">
            </Col>
         </Row>
      </Container>
   ) : (
      <Container className=" my-5 pt-5 errorPage">
         <div className="errorImg"></div>
         <div className="text-center d-flexError">
            <h1>OOPS!</h1>
            <h3>We can't seem to find the page you're looking for.</h3>
            <h6>Error code: 404</h6>
            <a class="btn btn-airgo btn-lg px-3" href="/" role="button">
               Back to Home Page
            </a>
         </div>
      </Container>
   );
}
