import React from 'react';
import BannerService from './../components/BannerService';
import Types from './../components/Types';
import Services from './../components/Services';
import Brands from './../components/Brands';
import Footer from './../components/Footer';
import {Container} from 'react-bootstrap';

export default function OurServices() {
	return (
		<Container fluid className="p-0 servicesPage">
			<BannerService/>
			<Types/>
			<Services />
			<Brands />
			<Footer/>
		</Container>
		)
}